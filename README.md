# androcker

Run Docker natively on Android

## Oneplus 5T

A Oneplus 5T will be used to run Docker on Android.

## Sources

- https://www.getdroidtips.com/aosp-android-12-oneplus-5/
- https://gist.github.com/FreddieOliveira/efe850df7ff3951cb62d74bd770dce27
